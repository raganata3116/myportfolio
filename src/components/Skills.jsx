import React from 'react';

const Skills = () => {
    return (
        <div name='skills' className='w-full h-screen bg-[#0a192f] text-white-300'>
            {/* Container */}
            <div className='max-w-[1000px] mx-auto p-4 flex flex-col justify-center w-full h-full'>
                <div className='w-full flex justify-center items-center flex-col mb-7'>
                    <p className="text-4xl font-bold inline border-b-4 border-cyan-500 text-white text-center">Skills</p>
                    <p className="py-4 text-2xl text-white">I enjoy diving into and learning new things. Here's a list of technologies I've worked with</p>
                </div>
                <div className="w-full grid grid-cols-2 sm:grid-cols-4 gap-4 text-center py-8">
                    <div className="shadow-md shadow-[#ffff] hover:scale-110 duration-500">
                        <p className="my-4 text-white">HTML</p>
                    </div>
                    <div className="shadow-md shadow-[#ffff] hover:scale-110 duration-500">
                        <p className="my-4 text-white">CSS</p>
                    </div>
                    <div className="shadow-md shadow-[#ffff] hover:scale-110 duration-500">
                        <p className="my-4 text-white">PHP</p>
                    </div>
                    <div className="shadow-md shadow-[#ffff] hover:scale-110 duration-500">
                        <p className="my-4 text-white">Javascript</p>
                    </div>
                    <div className="shadow-md shadow-[#ffff] hover:scale-110 duration-500">
                        <p className="my-4 text-white">React JS</p>
                    </div>
                    <div className="shadow-md shadow-[#ffff] hover:scale-110 duration-500">
                        <p className="my-4 text-white">CodeIgniter 3</p>
                    </div>
                    <div className="shadow-md shadow-[#ffff] hover:scale-110 duration-500">
                        <p className="my-4 text-white">MySQL</p>
                    </div>
                    <div className="shadow-md shadow-[#ffff] hover:scale-110 duration-500">
                        <p className="my-4 text-white">Figma</p>
                    </div>

                </div>
            </div>
        </div>
    )
}
export default Skills;