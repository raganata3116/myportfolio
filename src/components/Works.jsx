import React from 'react';
import code1 from '../assets/detak.png';
import code2 from '../assets/ruangbelajar.png';
import code3 from '../assets/crowde.png';
import code4 from '../assets/sipeg.png';
import code5 from '../assets/speak.png';
import code6 from '../assets/lamkprs.png';



const Works = () => {
    return (
        <div name='work' className='w-full md:h-screen text-gray-300 bg-[#0a192f]'>
            <div className='max-w-[1000px] mx-auto p-4 flex flex-col justify-center w-full h-full'>
                <div className='pb-8 w-full flex justify-center items-center flex-col'>
                    <p className='text-4xl font-bold inline border-b-4 text-gray-300 border-cyan-500'>
                        Work
                    </p>
                    <p className='py-6 text-2xl'>Check out some of my most recent work</p>
                </div>
                {/* Container */}
                <div className='grid sm:grid-cols-2 md:grid-cols-3 gap-4'>
                    {/* Grid Item */}
                    <div
                        style={{ backgroundImage: `url(${code1})` }}
                        className='shadow-lg shadow-[#040c16] group container rounded-md flex justify-center items-center mx-auto content-div'
                    >
                        {/* Hover Effects */}
                        <div className='opacity-0 group-hover:opacity-100 flex justify-center items-center flex-col'>
                            <span className=' text-lg font-bold text-white tracking-wider text-center'>
                                Company Profile
                            </span>
                            <p className='text-center'>DETAK KOMUNIKA tbk merupakan Perusahaan yang bergerak di bidang Jasa Periklanan. Di bangun dengan PHP Native dan Framework Bootstrap</p>
                            <div className='pt-8 text-center'>
                                <a href='https://detak.nasihosting.com/' target='_blank' rel="noreferrer">
                                    <button className='text-center rounded-lg px-4 py-3 m-2 bg-white text-gray-700 font-bold text-lg'>
                                        Demo
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div
                        style={{ backgroundImage: `url(${code2})` }}
                        className='shadow-lg shadow-[#040c16] group container rounded-md flex justify-center items-center mx-auto content-div'
                    >
                        {/* Hover Effects */}
                        <div className='opacity-0 group-hover:opacity-100 flex justify-center items-center flex-col'>
                            <span className=' text-lg font-bold text-white tracking-wider text-center'>
                                E-Learning UI/UX
                            </span>
                            <p className='text-center'>Ruang Belajar merupakan salah satu E-Learning yang di khususkan untuk kalangan anak sekolah tingkat SD s/d SMA/K.</p>
                            <div className='pt-8 text-center'>
                                <a href='https://www.figma.com/proto/7g4KWVqbKUfffBtxM69aZN/SVI---Workshop-%237?page-id=48%3A3&type=design&node-id=51-886&viewport=55%2C393%2C0.16&t=nyoSr4EOwQZ7JLwu-1&scaling=scale-down&starting-point-node-id=51%3A886&mode=design' target='_blank' rel='noreferrer'>
                                    <button className='text-center rounded-lg px-4 py-3 m-2 bg-white text-gray-700 font-bold text-lg'>
                                        Demo
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div
                        style={{ backgroundImage: `url(${code3})` }}
                        className='shadow-lg shadow-[#040c16] group container rounded-md flex justify-center items-center mx-auto content-div'
                    >
                        {/* Hover Effects */}
                        <div className='opacity-0 group-hover:opacity-100 flex justify-center items-center flex-col'>
                            <span className=' text-lg font-bold text-white tracking-wider text-center'>
                                Website Crowde UI/UX
                            </span>
                            <p className='text-center'>PT Crowde Membangun Bangsa merupakan perusahaan yang bergerak di bidang pertanian dengan Bisnis Fintech P2P yang di tawarkan.</p>
                            <div className='pt-8 text-center'>
                                <a href='https://www.figma.com/proto/HRtyuiLo8De2fWi6ZgHnWf/UIX-13---Kelompok-5---Crowde?page-id=11%3A3&type=design&node-id=153-220&viewport=337%2C117%2C0.03&t=QcmekBj4rqIR0XRC-1&scaling=min-zoom&starting-point-node-id=153%3A220&mode=design' target='_blank' rel="noreferrer">
                                    <button className='text-center rounded-lg px-4 py-3 m-2 bg-white text-gray-700 font-bold text-lg'>
                                        Demo
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div
                        style={{ backgroundImage: `url(${code4})` }}
                        className='shadow-lg shadow-[#040c16] group container rounded-md flex justify-center items-center mx-auto content-div'
                    >
                        {/* Hover Effects */}
                        <div className='opacity-0 group-hover:opacity-100 flex justify-center items-center flex-col'>
                            <span className=' text-lg font-bold text-white tracking-wider text-center'>
                                SIPEG LAM-KPRS
                            </span>
                            <p className='text-center'>Sistem Kepegawaian Internal Lembaga Akreditasi Mutu dan Keselamatan Pasien Rumah Sakit (LAM-KPRS).</p>
                            <div className='pt-8 text-center'>
                                <a href='https://sipeg.lam-kprs.id/' target='_blank' rel='noreferrer'>
                                    <button className='text-center rounded-lg px-4 py-3 m-2 bg-white text-gray-700 font-bold text-lg'>
                                        Demo
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div
                        style={{ backgroundImage: `url(${code5})` }}
                        className='shadow-lg shadow-[#040c16] group container rounded-md flex justify-center items-center mx-auto content-div'
                    >
                        {/* Hover Effects */}
                        <div className='opacity-0 group-hover:opacity-100 flex justify-center items-center flex-col'>
                            <span className=' text-lg font-bold text-white tracking-wider text-center'>
                                SPEAK LAM-KPRS
                            </span>
                            <p className='text-center'>Sistim ini digunakan untuk mengelola semua Data Akreditasi Rumah Sakit dan Surveior yang dilakukan oleh LAM-KPRS.</p>
                            <div className='pt-8 text-center'>
                                <a href='https://speak.lam-kprs.id/' target='_blank' rel='noreferrer'>
                                    <button className='text-center rounded-lg px-4 py-3 m-2 bg-white text-gray-700 font-bold text-lg'>
                                        Demo
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div
                        style={{ backgroundImage: `url(${code6})` }}
                        className='shadow-lg shadow-[#040c16] group container rounded-md flex justify-center items-center mx-auto content-div'
                    >
                        {/* Hover Effects */}
                        <div className='opacity-0 group-hover:opacity-100 flex justify-center items-center flex-col'>
                            <span className=' text-lg font-bold text-white tracking-wider text-center'>
                                Website LAM-KPRS
                            </span>
                            <p className='text-center'>Website Utama LAM-KPRS merupakan sarana seluruh RS di Indonesia dalam melakukan pengajuan Akreditasi dan Bimbingan.</p>
                            <div className='pt-8 text-center'>
                                <a href='https://lam-kprs.id/' target='_blank' rel='noreferrer'>
                                    <button className='text-center rounded-lg px-4 py-3 m-2 bg-white text-gray-700 font-bold text-lg'>
                                        Demo
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    );
};
export default Works;